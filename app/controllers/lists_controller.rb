class ListsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: [:edit, :update, :destroy]

  def show
    @lists = current_user.lists
    @list = current_user.lists.find_by(id: params[:id])
    @list_tasks = @list.task.paginate(page: params[:page])
  end

  def new
    @list = List.new
  end

  def create
    @list = current_user.lists.new(list_params)
    if @list.save
      flash[:success] = "List created!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @list = current_user.lists.find_by(id: params[:id])
  end

  def update
    if @list.update_attributes(list_params)
      flash[:success] = "List updated"
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def destroy
    @list.destroy
    flash[:success] = "List deleted"
    redirect_to current_user
  end

private

  def list_params
    params.require(:list).permit(:title, :description, :user_id)
  end

  def correct_user
      @list = current_user.lists.find_by(id: params[:id])
      redirect_to root_url if @list.nil?
    end
end
