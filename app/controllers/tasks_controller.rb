class TasksController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: [:edit, :update, :destroy]
  include TasksHelper

  def new
    @task = Task.new
  end

  def create
    @task = current_user.tasks.new(task_params)
    if @task.save
      @task_in_list = Relationship.new(list_params)
      @task_in_list.task_id = @task.id
      if @task_in_list.save
        flash[:success] = "Task created!"
        redirect_to current_user
      else
        render 'new'
      end
    else
      render 'new'
    end
  end

  def edit
    @task = current_user.tasks.find_by(id: params[:id])
  end

  def update
    if @task.update_attributes(task_params)
      @task_in_list = Relationship.find_by(task_id: params[:id])
      if @task_in_list.update_attributes(list_params)
        flash[:success] = "Task updated"
        redirect_to current_user
      else
        redirect_to current_user
      end
    else
      render 'edit'
    end
  end

  def destroy
    @task.destroy
    flash[:success] = "Task deleted"
    redirect_to request.referrer || current_user
  end

  def status_complite
    @task = current_user.tasks.find_by(id: params[:task_id])
    @task.update_attribute(:status, true)
    flash[:success] = "Status updated"
    redirect_to current_user
  end

  def change_status
    @task = current_user.tasks.find_by(id: params[:task_id])
    @task.update_attribute(:status, false)
    flash[:success] = "Status updated"
    redirect_to current_user
  end
private

  def task_params
    params.require(:task).permit(:title, :content, :user_id, :status)
  end

  def list_params
    params.require(:list).permit(:list_id, :task_id)
  end

    def correct_user
      @task = current_user.tasks.find_by(id: params[:id])
      redirect_to root_url if @task.nil?
    end
end
