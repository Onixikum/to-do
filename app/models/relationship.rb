class Relationship < ApplicationRecord
  belongs_to :task
  belongs_to :list

  validates :list_id, presence: true
  validates :task_id, presence: true
end
