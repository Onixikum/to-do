class List < ApplicationRecord
  belongs_to :user
  has_many :relationships
  has_many :task, through: :relationships, dependent: :destroy

  default_scope -> { order(updated_at: :desc) }
  validates :user_id, presence: true
  validates :title, presence: true, length: { minimum: 3, maximum: 16 }
  validates :description, presence: true, length: { minimum: 5, maximum: 100 }

end
