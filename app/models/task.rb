class Task < ApplicationRecord
  belongs_to :user
  has_many :relationships
  has_many :list, through: :relationships, dependent: :destroy

  default_scope -> { order(updated_at: :desc) }
  validates :user_id, presence: true
  validates :title,   presence: true, length: { maximum: 50 }
  validates :content, presence: true, length: { maximum: 250 }

end
