Rails.application.routes.draw do
  root   'static_page#home'
  get    '/signup',  to: 'users#new'
  post   '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users, shallow: true do
    resources :lists
    resources :tasks do
      get :status_complite
      get :change_status
    end
  end
end
