User.create!(name:                  "Satoshi Nakamoto",
             email:                 "test@mail.com",
             password:              "xxxxxxxx",
             password_confirmation: "xxxxxxxx")

10.times do |n|
  name = Faker::Name.name
  email = "test-#{n+1}@mail.com"
  password = "password"
  User.create!(name:                  name,
               email:                 email,
               password:              password,
               password_confirmation: password)
end

user = User.order(:updated_at).take(5)
5.times do |n|
  title = "List-#{n+1}"
  description = Faker::Lorem.sentence(word_count: 5)
  user.each { |user| user.lists.create!(title: title, description: description) }
end

user = User.order(:updated_at).take(5)
25.times do
  title = Faker::Job.title
  content = Faker::Lorem.sentence(word_count: 25)
  user.each { |user| user.tasks.create!(title: title, content: content) }
end


