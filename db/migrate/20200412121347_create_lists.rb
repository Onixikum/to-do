class CreateLists < ActiveRecord::Migration[5.2]
  def change
    create_table :lists do |t|
      t.text :title
      t.text :description
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :lists, [:user_id, :updated_at]
  end
end
