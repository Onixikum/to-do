class CreateRelationships < ActiveRecord::Migration[5.2]
  def change
    create_table :relationships do |t|
      t.integer :list_id
      t.integer :task_id

      t.timestamps null: false
    end

    add_index :relationships, :list_id
    add_index :relationships, :task_id
    add_index :relationships, [:list_id, :task_id], unique: true
  end
end
